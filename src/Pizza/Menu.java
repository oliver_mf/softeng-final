package src.Pizza;

import java.util.ArrayList;

public class Menu {

    private ArrayList<Topping> toppingList;
    private ArrayList<Recipe> recipeList;
    private ArrayList<Recipe> foundSecretList;
    private boolean secretFound;

    public Menu() {
        this.toppingList = new ArrayList<>();
        this.recipeList = new ArrayList<>();
        this.foundSecretList = new ArrayList<>();
        this.secretFound = false;
    }

    public ArrayList<Topping> getToppingList() {
        return toppingList;
    }

    public ArrayList<Recipe> getRecipeList() {
        return recipeList;
    }

    public ArrayList<Recipe> getFoundSecretList() {
        return foundSecretList;
    }

    public boolean getSecretFound() {
        return secretFound;
    }

    public void setSecretFound(boolean bool) {
        this.secretFound = bool;
    }

    public void addTopping(Topping topping) {
        this.toppingList.add(topping);
    }

    public void addRecipe(Recipe recipe) {
        this.recipeList.add(recipe);
    }

    public void addSecret(Recipe recipe) {
        if (!this.foundSecretList.contains(recipe)) {
            this.foundSecretList.add(recipe);
        }
    }

    public boolean hasTopping(String toppingName, int price) {
        for (int i = 0; i < this.toppingList.size(); i++) {
            if (this.toppingList.get(i).getName().equals(toppingName) && this.toppingList.get(i).getPrice() == price) {
                return true;
            }
        }
        return false;
    }

    public boolean hasRecipe(String recipeName, int price) {
        for (int i = 0; i < this.recipeList.size(); i++) {
            if (this.recipeList.get(i).getName().equals(recipeName) && this.recipeList.get(i).getPrice() == price) {
                return true;
            }
        }
        return false;
    }
}
