package src.Pizza;

import java.util.HashMap;
import java.util.Map;

public class Order {
    
    private Map<Topping, Integer> toppingMap;
    
    public Order() {
        this.toppingMap = new HashMap<Topping, Integer>();
    }

    public Map<Topping, Integer> getToppingMap() {
        return toppingMap;
    }

    public void addTopping(Topping topping, Integer amount) {
        this.toppingMap.put(topping, amount);
    }

    public boolean compareRecipe(Recipe recipe) {
        int matchCount = 0;
        if (this.getToppingMap().size() == recipe.getToppingMap().size()) {
            for (Topping recipeTopping: recipe.getToppingMap().keySet()) {
                for (Topping orderTopping: this.getToppingMap().keySet()) {
                    if (recipeTopping.getName().equals(orderTopping.getName()) && (recipe.getToppingMap().get(recipeTopping) == this.getToppingMap().get(orderTopping))) {
                        matchCount++;
                        break;
                    }
                }
            }
        }
        if (matchCount == this.getToppingMap().size()) {
            return true;
        }
        return false;
    }

    public int getPrice() {
        int orderPrice = 0;
        for (Topping key: this.getToppingMap().keySet()) {
            orderPrice += key.getPrice()*this.getToppingMap().get(key);;
        }
        return orderPrice;
    }

    public boolean hasTopping(String toppingName) {
        for (Topping key: this.getToppingMap().keySet()) {
            if (key.getName().equals(toppingName)) {
                return true;
            }
        }
        return false;
    }
}