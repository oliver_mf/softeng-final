package src.Pizza;

import java.util.HashMap;
import java.util.Map;

public class Recipe {

    private String name;
    private int price;
    private Map<Topping, Integer> toppingMap;

    public Recipe() {}
    
    public Recipe(String name, int price) {
        this.name = name;
        this.price = price;
        this.toppingMap = new HashMap<Topping, Integer>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Map<Topping, Integer> getToppingMap() {
        return toppingMap;
    }

    public void addTopping(Topping topping, Integer amount) {
        this.toppingMap.put(topping, amount);
    }
}
