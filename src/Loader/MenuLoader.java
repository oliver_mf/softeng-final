package src.Loader;

import src.Pizza.*;

import java.io.IOException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class MenuLoader {

    private Menu menu;
    private int n;

    public MenuLoader() {
        this.menu = new Menu();
    }

    public Menu loadFile() throws IOException {
        URL path = MenuLoader.class.getResource("input.txt");
        File file = new File(path.getFile());
        BufferedReader br = new BufferedReader(new FileReader(file));

        //topping input
        String line = br.readLine();
        String[] lineSplit = line.split(" ");
        if (!lineSplit[0].equals("topping")) {
            throw new IOException("false input method");
        }
        n = Integer.parseInt(lineSplit[1]);
        for (int i = 0; i < n; i++) {
            String toppingIn= br.readLine();
            String[] toppingSplit = toppingIn.split(" ");
            Topping tempTopping = new Topping(toppingSplit[0], Integer.parseInt(toppingSplit[1]));

            for (int j = 0; j < menu.getToppingList().size(); j++) {
                if (tempTopping.getName().equals(menu.getToppingList().get(j).getName())) {
                    throw new IOException("duplicate topping!");
                }
            }

            menu.addTopping(tempTopping);
        }


        //secret recipe input
        line = br.readLine();
        lineSplit = line.split(" ");
        if (!lineSplit[0].equals("recipe")) {
            throw new IOException("false input method");
        }
        n = Integer.parseInt(lineSplit[1]);
        for (int i = 0; i < n; i++) {
            String recipeIn = br.readLine();
            String[] recipeSplit = recipeIn.split(", ");
            String[] numSplit = recipeSplit[1].split(" ");
            Recipe tempRecipe = new Recipe(recipeSplit[0], Integer.parseInt(numSplit[1]));

            for (int j = 0; j < menu.getRecipeList().size(); j++) {
                if (tempRecipe.getName().equals(menu.getRecipeList().get(j).getName())) {
                    throw new IOException("duplicate secret recipe!");
                }
            }
            
            //add recipe toppings
            for (int j = 0; j < Integer.parseInt(numSplit[0]); j++) {
                String toppingIn = br.readLine();
                String[] toppingSplit = toppingIn.split(" ");
                for (int k = 0; k < menu.getToppingList().size(); k++) {
                    if (toppingSplit[0].equals(menu.getToppingList().get(k).getName())) {
                        tempRecipe.addTopping(menu.getToppingList().get(k), Integer.parseInt(toppingSplit[1]));
                    }
                }
            }
            menu.addRecipe(tempRecipe);
        }

        br.close();
        return menu;
    }
}