package src.Printer;

import src.Pizza.Menu;
import src.Pizza.Order;
import src.Pizza.Recipe;
import src.Pizza.Topping;

public class Printer {
    
    public Printer() {
    }

    public void printOrder(Order order, int orderNum) {
        System.out.print("Order " + orderNum + ": ");
        for (Topping key: order.getToppingMap().keySet()) {
            System.out.print(key.getName().toUpperCase() + "x" + order.getToppingMap().get(key) + " ");
        }
        System.out.println("- Price: " + order.getPrice() + "$\n");
    }

    public void printRecipe(Recipe recipe,int orderNum) {
        System.out.println("Order " + orderNum + ": " + recipe.getName().toUpperCase() + " - Price: " + recipe.getPrice() + "$\n");
    }

    public void printTotal(int totalPrice) {
        System.out.println("Total Price: " + totalPrice + "$");
    }

    public void printAvailableToppings(Menu menu) {
        System.out.println("Available toppings:");
        for (int i = 0; i < menu.getToppingList().size(); i++) {
            System.out.println("- " + menu.getToppingList().get(i).getName() + "  Price: " + menu.getToppingList().get(i).getPrice() + "$");
        }
    }

    public void printFoundSecret(Menu menu) {
        System.out.println("You have found secret recipe(s)!");
        for (int i = 0; i < menu.getFoundSecretList().size(); i++) {
            System.out.println("- " + menu.getFoundSecretList().get(i).getName() + "  Price: " + menu.getFoundSecretList().get(i).getPrice() + "$");
        }
    }
}