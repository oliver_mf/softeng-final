package src.Test;
import src.Loader.*;
import src.Pizza.*;
import src.Printer.Printer;

public class PizzaShopTest {
    public static void main(String[] args) throws Exception {
        try {
            //Test load menu inputs
            MenuLoader loader = new MenuLoader();
            Menu menu = loader.loadFile();
    
            System.out.println("Testing menu topping inputs...");
            if (menu.hasTopping("cheese", 7) && menu.hasTopping("mushroom", 4) && menu.hasTopping("pepperoni", 5)) {
                    System.out.println("...menu topping test passed SUCCESSFULLY!\n");
                } else {
                    System.out.println("...menu topping test FAILED!");
                    System.exit(-1);
                }
            
            System.out.println("Testing secret recipe inputs...");
            if (menu.hasRecipe("triple cheese", 18) && menu.hasRecipe("mushroom delight", 13)) {
                System.out.println("...secret recipe test passed SUCCESSFULLY!\n");
            } else {
                System.out.println("...secret recipe test FAILED!");
            }

            //Test customer order inputs
            Order order1 = new Order();
            System.out.println("Testing order topping inputs...");
            order1.addTopping(menu.getToppingList().get(0), 1);
            order1.addTopping(menu.getToppingList().get(1), 2);
            if (order1.hasTopping("cheese") && order1.hasTopping("mushroom")) {
                System.out.println("...order topping test passed SUCCESSFULLY\n");
            } else {
                System.out.println("...order topping test FAILED");
            }

            System.out.println("Testing order and secret recipe topping comparison...");
            if (order1.compareRecipe(menu.getRecipeList().get(1))) {
                menu.setSecretFound(true);
                menu.addSecret(menu.getRecipeList().get(1));
                System.out.println("...recipe matching test passed SUCCESSFULLY\n");
            } else {
                System.out.println("...recipe matching test FAILED");
            }

            //Test order printer
            System.out.println("Testing order price printer...");
            Printer printer = new Printer();
            Order order2 = new Order();
            int totalPrice = 0;
            order2.addTopping(menu.getToppingList().get(0), 1);
            order2.addTopping(menu.getToppingList().get(2), 1);
            totalPrice += menu.getRecipeList().get(1).getPrice();
            totalPrice += order2.getPrice();
            printer.printRecipe(menu.getRecipeList().get(1), 1);
            printer.printOrder(order2, 2);
            printer.printTotal(totalPrice);
            System.out.println("...order price printer test passed SUCCESSFULLY");
            
            //Test view available toppings
            System.out.println("Testing view available toppings...");
            printer.printAvailableToppings(menu);
            System.out.println("...view available toppings test passed SUCCESSFULLY");

            //Test view found secret recipes
            System.out.println("Testing view found secret recipes...");
            if (menu.getSecretFound()) {
                printer.printFoundSecret(menu);
            } else {
                System.out.println("You haven't found any secret...");
            }
            System.out.println("...view found secret recipes passed SUCCESSFULLY");
        } catch (Exception e) {
            System.out.println("Something went wrong...");
        }
    }
}