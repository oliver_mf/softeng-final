package src.Main;
import src.Pizza.*;
import src.Printer.Printer;
import src.Loader.*;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.StringTokenizer;

public class PizzaShop {

    private static InputReader in;
    private static MenuLoader loader = new MenuLoader();
    private static Printer printer = new Printer();

    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        in = new InputReader(inputStream);

        readInput();
    }

    private static void readInput() throws IOException {
        //load menu
        Menu menu = loader.loadFile();

        //read commands
        boolean run = true;
        while (run) {
            System.out.println("\nType 'help' to see command list");
            System.out.print("Type in your command: ");
            String command = in.next();

            switch (command) {
                case "help":
                    System.out.println("Commands: ");
                    System.out.println("View menu = menu");
                    System.out.println("Order pizza = order");
                    System.out.println("Secret Recipe??? = secret");
                    System.out.println("Exit = exit");
                    break;

                case "menu":
                    printer.printAvailableToppings(menu);
                    break;

                case "order":
                    //costumer's order
                    int orderNum = 0;
                    boolean orderFlag = true;
                    int totalPrice = 0;
                    while (orderFlag) {
                        System.out.println("Insert your order (topping1 amount1, topping2 amount2, ...)");
                        orderNum++;
                        boolean validTopping = true;
                        Order newOrder = new Order();

                        while (validTopping) {
                            String orderTopping = in.nextLine();
                            String[] toppingSplit = orderTopping.split(", ");
                            int validToppingCount = 0;

                            //add order topping
                            for (int j = 0; j < toppingSplit.length; j++) {
                                String[] amountSplit = toppingSplit[j].split(" ");
                                for (int k = 0; k < menu.getToppingList().size(); k++) {
                                    if (amountSplit[0].equals(menu.getToppingList().get(k).getName())) {
                                        newOrder.addTopping(menu.getToppingList().get(k), Integer.parseInt(amountSplit[1]));
                                        validToppingCount++;
                                    }
                                }
                            }

                            //check if all order's topping are available
                            if (validToppingCount == toppingSplit.length) {
                                validTopping = false;
                            } else {
                                System.out.println("Topping(s) unavailable! Please enter again");
                            }
                        }

                        //compare order with available recipes
                        boolean matchFlag = false;
                        for (int j = 0; j < menu.getRecipeList().size(); j++) {
                            if (newOrder.compareRecipe(menu.getRecipeList().get(j))) {
                                matchFlag = true;
                                totalPrice += menu.getRecipeList().get(j).getPrice();
                                printer.printRecipe(menu.getRecipeList().get(j), orderNum);
                                menu.setSecretFound(true);
                                menu.addSecret(menu.getRecipeList().get(j));
                                break;
                            }
                        }
                        if (!matchFlag) {
                             totalPrice += newOrder.getPrice();
                            printer.printOrder(newOrder, orderNum);;
                        }

                        //check input of add more order
                        boolean ynFlag = true;
                        System.out.print("Add more(y/n)?: ");
                        while (ynFlag) {
                            String moreCheck = in.next();
                            if (moreCheck.equals("y")) {
                                orderFlag = true;
                                ynFlag = false;
                            } else if (moreCheck.equals("n")) {
                                orderFlag = false;
                                ynFlag = false;
                            } else {
                                System.out.print("Invalid input! Please enter (y/n): ");
                                continue;
                            }
                        }
                    }
                    printer.printTotal(totalPrice);
                    System.out.println("Thank you for ordering Lorenzo's Pizza!");
                    break;

                case "secret":
                    if (menu.getSecretFound()) {
                        printer.printFoundSecret(menu);
                        break;
                    } else {
                        System.out.println("You haven't found any secret...");
                        break;
                    }
                
                case "exit":
                    System.out.println("Enjoy Your Pizza!");
                    run = false;
                    break;
            }
        }
    }

    static class InputReader {
        // taken from https://codeforces.com/submissions/Petr
        public BufferedReader reader;
        public StringTokenizer tokenizer;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream), 32768);
            tokenizer = null;
        }

        public String next() {
            while (tokenizer == null || !tokenizer.hasMoreTokens()) {
                try {
                    tokenizer = new StringTokenizer(reader.readLine());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            return tokenizer.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(next());
        }

        public String nextLine() throws IOException {
            return reader.readLine();
        }
    }
}